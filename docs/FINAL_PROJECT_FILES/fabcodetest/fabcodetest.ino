#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "RTClib.h"

RTC_DS3231 rtc;
char *daysOfTheWeek[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET 4
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
int loadingbarX=6, loadingbarY=56, loadingbarWidth=116, loadingbarHeight=8;
float literCounter=0;
float literCounterDivider=15;
float averageShower=15.0;
float ecoShower=12.0;
bool cling=true;
int rawVoltPin = A2;
int rawVolt = 0;  // variable to store the value read
unsigned long chipTime=0.0;

void setup() {
  Wire.setClock(300000); //Baudrate augmentation otherwise multiple I2C communication is slow. (standard=100000, atmega328p has a max baudrate of 400000)
  Serial.begin(9600);
  if (! rtc.begin()) {
    Serial.begin(9600);
    Serial.println("Couldn't find RTC");
    while (1);
  }
  if (rtc.lostPower()) {
    Serial.begin(9600);
    Serial.println("RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)// Check your I2C address and enter it here, in Our case address is 0x3C
  display.clearDisplay();
  display.display(); // this command will display all the data which is in buffer
  display.setTextColor(WHITE, BLACK);
  display.drawRect(37, 17, 3, 3, WHITE);     // Put degree symbol ( ° )
  draw_text(42, 17, "C", 1);
  //Draw loading bar
  display.drawRect(loadingbarX, loadingbarY, loadingbarWidth, loadingbarHeight, WHITE);
  char bufferShower[32];
  
  draw_text(68,19,"Shower:",1);
  display.setCursor(68,28);
  display.setTextSize(1);
  display.print("Avg=");
  display.print(averageShower,1);
  display.print("L");
  display.setCursor(68,37);
  display.setTextSize(1);
  display.print("Eco=");
  display.print(ecoShower,1);
  display.print("L");
  // Draw the indicator on the loading bar
  
  if(averageShower>=ecoShower){
    literCounterDivider=averageShower;
    ecoShower=(ecoShower/averageShower*loadingbarWidth-1);
    if(loadingbarWidth>ecoShower+5){
      draw_text(loadingbarWidth+3,47,"A",1);
      draw_text(ecoShower-2,47,"E",1);
      display.fillRect(ecoShower, loadingbarY+1, 1 , loadingbarHeight-2, WHITE);
    }else{
      draw_text(loadingbarWidth+3,47,"A",1);
    }
  }else{
    literCounterDivider=ecoShower;
    averageShower=(averageShower/ecoShower*loadingbarWidth-1);
    if(loadingbarWidth>averageShower+5){
      draw_text(loadingbarWidth+3,47,"E",1);
      draw_text(averageShower-2,47,"A",1);
      display.fillRect(averageShower, loadingbarY+1, 1 , loadingbarHeight-2, WHITE);
    }else{
      draw_text(loadingbarWidth+3,47,"E",1);
    }
  }
}
void loop() {
  rawVolt = analogRead(rawVoltPin);  // read the input pin
  unsigned long prevTime=chipTime;
  chipTime=millis();
  unsigned long difTime=0;
  if(prevTime!=0){
    difTime=chipTime-prevTime;
  }
  literCounter+=difTime*rawVolt/600000.0;
  DateTime now = rtc.now();
  Serial.println(now.second());
  Serial.println(now.minute());
  /*============Display Date=================*/
  draw_text(0,0,daysOfTheWeek[now.dayOfTheWeek()],1);
  char currentDate [16];
  sprintf (currentDate, "%02d/%02d/%d", now.day(), now.month(), now.year()); //add leading zeros to the day and month
  draw_text(68,0,currentDate,1);
  
  /*================Display Time================*/ 
  char buffer [16];
  sprintf (buffer, "%02d:%02d:%02d", now.hour(), now.minute(), now.second());
  draw_text(80,9,buffer,1);
  /*================Display Temp================*/
  display.setCursor(4,17);
  display.setTextSize(1);
  display.print(rtc.getTemperature());
  drawProgressbar(literCounter/literCounterDivider*100);
  display.setCursor(0, 27);
  display.setTextSize(2);
  if(literCounter>99.9){
    display.print("99.9");
  }else{
    display.print(literCounter,1);
  }
  display.print("L");
  display.display();
}

void draw_text(byte x_pos, byte y_pos, char *text, byte text_size) {
  display.setCursor(x_pos, y_pos);
  display.setTextSize(text_size);
  display.print(text);
  display.display();
}
void drawProgressbar(float progress)
{
   progress = progress > 100 ? 100 : progress; // set the progress value to 100
   progress = progress < 0 ? 0 :progress; // start the counting to 0-100
   float bar = ((float)(loadingbarWidth-4) / 100) * progress;
   if(cling && progress==100){
    cling=!cling;
    display.fillRect(loadingbarX+2, loadingbarY+2, bar , loadingbarHeight-4, WHITE);
   }else if(!cling && progress==100){
    cling=!cling;
    display.fillRect(loadingbarX+2, loadingbarY+2, bar , loadingbarHeight-4, BLACK);
   }else{
    display.fillRect(loadingbarX+2, loadingbarY+2, bar , loadingbarHeight-4, WHITE); // initailize the graphics fillRect(int x, int y, int width, int height)
   }
   
}
