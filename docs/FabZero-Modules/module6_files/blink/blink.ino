/*
 *   FILE    : blink.ino
 *   AUTHOR  : Andrew KARAM <karamyo@hotmail.com>
 *   DATE    : 2022-02-28
 *   license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/

const int led = 15;
const int blink_speed = 200;

void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(blink_speed);                       // wait for half a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(blink_speed);                       // wait for half a second
}
