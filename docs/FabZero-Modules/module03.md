# 3. Découpe assistée par ordinateur

After a reintroduction to documenting, a new presentation about laser cutter has been introduced.
The presentation started with security measures followed by materials that are recommended to use and the one that are not. It followed by a presentation of the laser cuter machine that are available in the FabLab, the Epilog Fusion Pro 32 is the one me and my group got assigned on and that I'll use for my individual task, which is: realizing a kirigami.

## 3.1 Group assignment - Epilog Fusion Pro 32

Our first assignment this week was to choose a material and determine the best parameters for our specific machine (speed, power, kerf, ...)
We will work with svg files (Scalable Vector Graphics). The program we will use is InkScape, but we could also use OpenSCAD.

### 3.1.1 Create the test bench

The first step is to sketch the piece we want to cut. We decide to do a grid of 5x5 squares on InkScape.

To be able to use it with the laser cutter, we need to use a vectorial image. So we export it this way:

![ ](../images/module3/group/vectorial.jpg)

Next, we need to color each square outline. Each one will have a different color because we want to be able to test each combination of force and speed at once.

! [ ](../images/module3/group/colorsquare.jpg) IMAGE!!

We chose to use a gray gradient for our different colors.

We export our sketch into SVG. Using a USB key we open our file on the Epilog's computer.

### 3.1.2 Cut the test bench (first try)

We open the file and we go to *FILE>PRINT* and we select the machine.

![ ](../images/module3/group/fileprint.jpg) IMAGE!!

Next, we get into the Epilog's Software. We need now to set the parameters.

First, pushing on the sketch, we have the possibility to split this sketch by colors. Then, we can determine the parameters for each subpart (each part of the sketch having a different color).

For each one, we choose those parameters :

- **process type** : *vector*
- **speed** : we go from 10 to 90 (step of 20)
- **power** : we go from 10 to 90 (step of 20)

Next, we choose to go with orange cardboard. We put it into the laser cutter (careful to put it flat in the machine to keep a constant distance between the cardboard and the laser).

On the software, we check with the video how to place the sketch. After, we check that the borders are well placed (those borders defines the region the laser can go into).

We choose for the parameter **Auto Focus** : *plunger*. Then, we *print* to add the file to the Epilog machine.

SECURITY PHOTOS et MESURES (fumée, clé, fermer)

![ ](../images/module3/group/careful.jpg)

We choose the file on the Epilog machine and we start.

![ ](../images/module3/group/start.jpg)

After having cut the piece, we can delete the file from the machine.

![ ](../images/module3/group/test1.jpg)

The result is a bit disappointing, almost every square is simply totally cut through. It is simply because the cardboard is too thin and the laser set on a too high power.

### 3.1.3 Cut the test bench (second try)

For our second try, we decided to focus ourself on a small part of the power range.

![ ](../images/module3/group/drawing.jpg)

This time, the parameters are :

- **process type** : *vector*
- **speed** : we go from 30 to 90 (step of 15)
- **power** : we go from 5 to 25 (step of 5)

![ ](../images/module3/group/test2.jpg)

The result is much more conclusive. Some of the square we very loose and fell down as soon as we picked up the test bench, but we have much more samples to study. We can see that if we go too slow, it will inevitably cut through the paper.

### 3.1.4 cut the test bench (third try)

For the second part of the assignment, the Kirigamis, we still lack one important information, what is are the best parameters to be able to easily fold our cardboard without simply cutting it.

We designed a simple rectangle with 5 lines, each having different parameters. We chose those parameters based on our previous test bench, we selected the squares that were not cut through but still quite deeply cut.

![ ](../images/module3/group/PLI.jpg)

The parameters are :

- **Process type** : *vector*
- **Speed** : 30, 60, 75, 90, 90
- **Power** : 5, 10, 10, 10, 15

Every cut hangs on correctly, but the best parameters pair seems to be (90,10), as it seems to be a bit more resilient.


## 3.2 Individual task

### 3.2.1 Research

The first thing that was required to use a laser cutter such as the Epilog Fusion Pro 32 is to know how to use a vector drawing tool. The **inkscape** tool was adviced in class. After some time of manipulation I had all the knowledge to use it and realize a design.

### 3.2.2 Kirigami idea

The first thing that came to my mind was to design a palmtree. The design would be based on folding and cuting and would be constitute of a single piece.
Here is a link to an [Inkscape Tutorial](https://inkscape.org/fr/learn/tutorials/).

After some digital drawing using **procreate** on my tablet, the idea was schematized:
Here is a link to a tutorial on procreate on how to make symmetric drawing which is the main tool I used [procreate Tutorial](https://crea-tutorium.com/comment-faire-un-mandala-sur-procreate/)

![](../images/treefirstdraw.jpg)

### 3.2.3 Ways to bend material in both ways

Next thing was to do the final version with precise measures on inkscape using vectors. After some time of work the design was a success and I booked a schedule for cutting the piece. 

/!\ However a **problem** I didn't plan happened. Whenever you cut in material with a laser, material is burned and thus removed. Whenever the material is not totally perfored by the laser it can be calculated to just be digged enough to become bendable. This is schematized by the figure below:

![](../images/laser2pli.png)

The problem that I didn't plan is that while digging in the material in one direction it becomes bendable in only one way. The other bending direction fragilize the material and might destroy the matter (a bending becomes a cut). This is due to the huge displacement of connection between the particles on the outter radius. This can be understood from the pictures I schematized.

A possibility is to swap the material in the laser cutter and to dig in the other direction for bendings that are supposed to be bend in the other direction. The problem with this method is that precision is not the best, one way to get it better is to cut a square around the figure and use it as a measure. And with the camera of the epilog fusion, try to aim for exactly the same location.

Another possibility that I thought about but didn't try is to do a **gradient** cut around the bending lines, which allows the bending in both directions. The other downward of this method is the fragility of the material and it requires a great knowledge of the material cutted. Here below is a figure that schematize this concept:

![](../images/laser2plie.png)


### 3.2.4 First results

1. This is the final .svg file drawn with inkscape:

![](../images/treefinaldraw.png)

2. When cutted in a paper this is what it looks like:

![](../images/treecut.jpg)

3. Assembling steps is to bend the trunc of the palm tree and assembling them together:

![](../images/treestep.jpg)

4. And the **final result**:

![](../images/finaltree.jpg)

