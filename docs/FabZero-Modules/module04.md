# 4. Impression 3D

This week goeal is to learn how to use the 3D printers and their slicers. This will help us realize the limits of this technique and work around them.

## 4.1 3D printing limitants

The limits of the 3D printing are observable in various manners. Firstly the **support** to 3D print is usually hard to remove. The material you use must be well considered before. The time of printing is also a limiting factor.

The first model I printed was too fragile. The main rod of the flexible structure broke after some switching. A solution would have been to put *less tension* in the rod by either making the base larger or either making the rod shorter. 
I decided to use a third method, change the rod print and made it thicker. This was a bad idea. **Due to the same mechanism of flexion that caused problem in my laser cutting section**, the material becomes fragilized when bended while being too thickn. This is due to the huge displacement of connection between the particles on the outter radius.
A less thick rod design helped to give more fluidity on the flexion of the rod, this was the solution I used eventhough the two first explanation were also working.

The design of the 3D print has been thought to have no bridges and thus no external structures were needed to complete the printing. This helped in cleanness of the printing but also in printing time.

## 4.2 PrusaSlicer

To go from a 3D model to a real world version of the design the printer must know how to print it. This is where slicer take part, they give the printer the exact movement they have to do for each floor of the printing to realize the model. 
PrusaSlicer has a lot of options for printing, these options goes from what shape the filling must be like to how many full layers are necessary at the bottom or at the top of the print. 
Everything is perfectly described in the PrusaSlicer documentation:
- [PrusaSlicer Documentation](https://help.prusa3d.com/en/article/first-print-with-prusaslicer_1753)

### 4.2.1 Basic PrusaSlicer settings

The main settings that you have to know are the layer height, which ideally you want to set at the same dimension of the nozzle installed on your 3D printer. If you try to put a higher layer height compared to your nozzle dimension, the filament flow is gonna be faster and makes less precise print. In reality, when you don't go too crazy with the settings, it usually works with no problems. /!\ this setting has an **extreme impact** on the printing time, it is **linearly linked**, if you want to print a piece with a height of 0.2 or 0.1 mm it takes twice the amount of time for the 0.1mm because it has to pass 2 times for the same amount of matter. 

The other important parameter is the **infill density**, for a solid structure don't go over 30 percents, otherwise it's just a waste. The different type of infills are well explained in the documentation link of Prusaslicer.

The third most important parameter (for me) is the **Generate support material**, in case you do long bridges in your 3D print, you have to use it. Typically you try to design pieces that won't require the support material because it gets the printed pieces corny when you peel it off.

## 4.3 Video compression

The prefered format for online video is MP4. Since I filmed with my phone, I realized facebook is already doing a fantastic job at compressing videos before they can be uploaded. I thus used it to transfer my videos from my phone to my computer and they are already compressed with an amazing result.

## 4.4 3D printed video

The result of the design I made in module 2:

<video controls muted>
<source src="../../images/module4/3D_print.mp4" type="video/mp4">
</video>

## 4.5 Completed FlexLink kit with a classmate work

By using and modifying the dimensions of Sajl work on his 3 axed flexbar and by modifying my previous work by adding a third anchor point. This gives an interesting result when the middle part switches, the furthest part also switch in the opposite way.

This gives a snake move alike. 

![ ](../images/module4/collab.png)

### 4.5.1 Code of modified classmate work

```

/*
FILE : flexlink_snake.scad

AUTHOR : Andrew KARAM

DATE OF MODIFICATION : 2022-03-31

FROM : Sajl Ghani, flexlink_dual.scad last modified on 2022-03-29 (https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/FabZero-Modules/module03/)

LICENSE : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

*/

$fn=100;

platform_width=1.0;
platform_diameter=100.0;
rotation_support_height=3.5;
rotation_support_diameter=4.8;
rotation_support_cap_ratio=5.0;

offset_circle=0;
width_circle=rotation_support_diameter+0.2;
width_circle_out=width_circle+2.0;
height=rotation_support_height-0.3;
flex_bar_width=1.5;
flex_bar_length=platform_diameter*6.0/8.0;
strum_ray=2.5;
strum_scale=1.5;

translate([0,-37.5,0])

union(){
translate([0,75,0])
    union(){    
//platform
difference(){
cylinder(h=platform_width,d=platform_diameter,center=true);
cylinder(h=platform_width,d=platform_diameter/2.0,center=true);
}
//rotation_support
union(){
    translate([0.0,platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,(rotation_support_height+rotation_support_height/rotation_support_cap_ratio)/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
    translate([0.0,-platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,rotation_support_height/2.0+rotation_support_height/rotation_support_cap_ratio/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
}
}
union(){    
//platform
difference(){
cylinder(h=platform_width,d=platform_diameter,center=true);
cylinder(h=platform_width,d=platform_diameter/2.0,center=true);
}
//rotation_support
union(){
    translate([0.0,platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,(rotation_support_height+rotation_support_height/rotation_support_cap_ratio)/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
    translate([0.0,-platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,rotation_support_height/2.0+rotation_support_height/rotation_support_cap_ratio/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
}
}
}

width=15.8;//mm the 2 extremities
depth=7;
height=3.2;
length_br=65;//mm la branche reliant les 2 extrémités

epaisseur=1;

hole_r=2.6;
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous
translate([60,-80,-0.5])
rotate([0,0,90])
union(){
difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}



translate([width,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};

translate([(width*2)+length_br,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([(width*2)+length_br+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};
}

```