# 1. Documentation

Since I had ideas for different projects I'd have liked to realize before beginning the fablab classes, I have decided to give me the time to leave myself get impregnated with the different techniques that are going to influence me throughout the weeks and nurish my ideas. Compliant mechanism is a good example of new perspectives I could use.

### Module objective:

This week objective is to learn how to document correctly our work and process to make sure no learning is being wasted and to help others to reproduce our work.

## 1.1 GIT

As mentionned in the index, I am a student in computer engineering. One of the main step of this first week is to discover git and gitlab, which, luckily for me, I was already familiar with. No researches were needed for this part.

I use virtual studio code as an IDE, with the git plugin add on and everything set up it's a real pleasure to work with git.
Here is the tutorial I followed in the past to set it up: 
- [VScode with git](https://code.visualstudio.com/docs/editor/versioncontrol)
And here is the link to download git for windows:
- [Download git for windows](https://git-scm.com/download/win)

Since virtual studio code and git both are owned by Microsoft, they made it really simple to interract with the two.

### 1.1.1 Git basic tutorial

After understanding what a SSH connection is and how to set it up, here is the basic commands that are the most useful for working with Git.

| Commands | Descriptions
| :----- | :-----
|`git clone` | Initialize you git project environment by cloning the global git repository locally to your computer.
|`git pull` | Download the current online project version.
| `git add -A` | Add all changes to the local git.
| `git commit -m "message"` | Put all the changes into a pocket called a commit which can be joined by a message.
| `git push` | Upload the commit to the server

If you want to go more into details about git, this is by far **the best tutorial** I have had:
- [Learning Git Branching](https://learngitbranching.js.org/?locale=fr_FR)

## 1.2 Markdown

With the same justification as in the Git section this is a tool I'm already familiar with.
Here is a cheatlist of the common function that can be used here: 
- [Markdown cheatsheet](https://www.markdownguide.org/cheat-sheet/)
These are the main commands I use:

| Commands | Descriptions
| :----- | :-----
|`#` | Everything that follows an hashtag on the same line is a title
|`##` | Same but for a subtitle
|`###` | Same but for a subsubtitle
| `**message**` | This allows to write bold messages
| `[message](url_link)` | Creates a clickable message that redirect to the url
| `![](image)` | Display an image from a folder source

## 1.3 ImageMagick

A tool to help reduce image size format, I am realizing this project on my windows exploitation system and not a linux one. Propositions were made to use the command line to reduce images size, which seems like a formidable tool but here I'll use their executable version. The executable version as the advantage of seeing directly the result of the transformation. If a picture is compressed too much and important details can't be understand anymore it is easy to go back and do a lower compression. 

### 1.3.1 Image Magick to reduce image size

1. First thing is to open an image:

![](../images/module1/open.png)

2. Select the image you want to reduce, I want to reduce a picture of me:

![](../images/module1/2chooseimage.png)

3. This opens the image you want to modify and allow you to observe the modification you apply to it in direct.

![](../images/module1/3.png)

4. After importing the image in "imagemagick" just click on the "view" index and than "Half Size". 

![](../images/module1/4view.png)

This divide the size of the image by 2 and allow to keep an observation of what the image looks like after the process. This is an advantage compared to the command line method because sometime you make it too small or unreadable and realize only once you open it.

## 1.4 SSH explanation

Sometimes images and sound works better than words this great tutorial is gonna explain to you way better than I could do by writing what SSH really is:
- [SSH explained](https://www.youtube.com/watch?v=z7jVOenqFYk&ab_channel=GitKraken)

## 1.5 Useful links

- [ImageMagick](https://imagemagick.org/script/index.php)
- [GitLab](https://about.gitlab.com/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
