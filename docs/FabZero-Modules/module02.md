# 2. Conception Assistée par Ordinateur

This week task has the goal of introducing us to **computer design conception**. More precisely the aim is to help us getting familiar with important 3D design program.
The two main programs that have been presented are **openScad** and **freeCAD**. 

Another goal of this week is to introduce 3D mechanisms and in particular **compliant mechanisms**. This opened my vision on a brand new field. 3D printing with rigid materials is not only about fixed structures. Indeed, the principle of compliant mechanism offers an infinite dimension of additional possibilities.

## 2.1 Concept worked on

### 2.1.1 Bistable

I'll explain the bistable principle in my own words and illustrate it with the design I realized on openScad here below.

A **bistable mechanism** is a mechanism based on compliant mechanism which can have **two stable position**. This means that thanks to a form of tension, the system is blocked into a stable position and by furnishing energy to it in the form of a work the system can land in another stable position where he would be trapped by a new tension applied to it.

The design I made is to illustrate the possibility of using a bistable mechanism to transform a rotation in a translation. It also shows how energy can be stored in this kind of mechanism for them to be more reactive whenever the energy has to be released. (Like a spring)

### 2.1.2 The concept

Since the transformation of a rotational movement to a linear movement is something I have already been struggling with in my past experience. I have focused myself on the design of a bistable mechanism that I slightly modified to make it changeable from one state to the other by the help of a joystick. This joystick represents a rotation movement and the switching from one state to another represents the linear movement.

### 2.1.3 Storing energy with compliant mechanisms

It also works **like a spring**, it is possible to store energy in this simple mechanism. If we don't switch the mechanism from one state to another but we get close to it, energy is stored. Just a few quantity of energy can be added to switch the state. This switching of state is releasing more energy than the quantity needed to finalize the switching.
I'll try to explain it better with the figure below.

![](../images/puits%20de%20potentiel.png)

This can be seen as a potential well. Where there is two stable position in b and -b and we can store the energy of work so that it doesn't bend enough to switch in the other stable position. This could be seen as reaching the potential V_o in the figure and staying there until we need the switch to occure.

Using this mechanism it allows us to deliver instantaneously a bigger quantity of energy. This could help to design a more responsive system.

## 2.2 OpenScad 

OpenScad is a free logicial under GPLv.2 licence that allows to design 3D shapes with code parametrically.
This means that it's based on a panel of forms and the interaction that they can have together.
A good cheatsheet is linked below in the useful link section and exemple of code I used that is commented is written below.
Here is a french tutorial on how to use it, illustrated by figures:
- [OpenScad tutorial](http://edutechwiki.unige.ch/fr/Tutoriel_OpenSCAD)

### 2.2.1 First code version

This version of code is non parametric, also the structure can be difficult to print. An update version is available at section 2.2.3.
```
/*
    FILE    : bendable_switch.scad
    AUTHOR  : Andrew KARAM <karamyo@hotmail.com>
    DATE    : 2022-02-28
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/

$fn=100;
depth=3.2;
union(){
rotate([0,90,0])
difference(){
cylinder(h=1,d=80,center=true);
cylinder(h=1,d=40,center=true);
    //cube([70,70,1], center=true); 
//cube([50,50,1], center=true);
}
translate([-(depth+1.5)/2,0,0])
union(){
translate([-2.4,30,0])
rotate([0,90,0])
cylinder(h=depth/3, d=5, center=true);

translate([-2.4,-30,0])
rotate([0,90,0])
cylinder(h=depth/3, d=5, center=true);
}
union(){
translate([-2.4,30,0])
rotate([0,90,0])
cylinder(h=depth+0.5, d=4.8, center=true);

translate([-2.4,-30,0])
rotate([0,90,0])
cylinder(h=depth+0.5, d=4.8, center=true);
}
}
translate([-(depth/2+1),0,0])
union(){
difference(){

cube([depth,60,1], center = true);
cube([depth-2,52,1], center=true);
}
translate([2.8,0,0])
scale([1.5,1,1]) difference(){
cylinder(h=1, d=6, center=true);
cylinder(h=1, d=4, center=true);
}
translate([0,-32.5,0])
rotate([90,0,90])
difference(){
cylinder(h=depth, d=7, center=true);
cylinder(h=depth, d=5, center=true);}
translate([-2*depth,-14.5,0])
rotate([90,0,0])
cylinder(h=29,d=depth,center=true);
translate([-1.5*depth,-32.5,0])
rotate([90,0,90])
difference(){
cylinder(h=2*depth, d=8, center=true);
translate([0,0,2])
cylinder(h=depth, d=6, center=true);}

translate([0,32.5,0])
rotate([90,0,90])
difference(){
cylinder(h=depth, d=7, center=true);
cylinder(h=depth, d=5, center=true);}
}
```

### 2.2.2 Image of the first 3D model

![](../images/bistable.png)


### 2.2.3 Update of the code

A better 3D printable version with no bridges and a more rigid link between the rings and the bar that flexes. Also the bar that flexes is more rigid and the strummer is smoothierly linked to the bar thanks to the "hull" function.

```
/*
    FILE    : bendable_switch.scad
    AUTHOR  : Andrew KARAM <karamyo@hotmail.com>
    DATE    : 2022-02-28
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/
$fn=100;

//Parameters for the base design

platform_width=1.0;
platform_diameter=100.0;
rotation_support_height=3.5;
rotation_support_diameter=4.8;
rotation_support_cap_ratio=5.0;

//Parameters for the flex bar

offset_circle=0;
width_circle=rotation_support_diameter+0.2;
width_circle_out=width_circle+2.0;
height=rotation_support_height-0.3;
flex_bar_width=1.5;
flex_bar_length=platform_diameter*6.0/8.0;
strum_ray=2.5;
strum_scale=1.5;

union(){    
    //1. platform
    difference(){
        cylinder(h=platform_width,d=platform_diameter,center=true);
        cylinder(h=platform_width,d=platform_diameter/2.0,center=true);
    }
    //1.1 rotation_support
    union(){
        translate([0.0,platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
        union(){
            translate([0,0,(rotation_support_height+rotation_support_height/rotation_support_cap_ratio)/2.0])
            cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
            cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
        }
        translate([0.0,-platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
        union(){
            translate([0,0,rotation_support_height/2.0+rotation_support_height/rotation_support_cap_ratio/2.0])
            cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
            cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
        }
    }
}

//2. Flexbar
translate([0,0,10]) 
union(){
    // 2.1 The bar that bends
    union(){
        cube([flex_bar_width,flex_bar_length,height], center = true);
        hull(){
            cube([flex_bar_width,strum_ray*3.0,height],center=true);
            translate([0,0,-strum_scale*strum_ray+height/2.0])
            scale([1,1,strum_scale])
            rotate([0,90,0])
            cylinder(h=1, d=strum_ray*2.0, center=true);
        }
    }
    // 2.2 It's circle support that will fixe to the base rotation support
    union(){
        translate([0,-(flex_bar_length+(width_circle_out+width_circle)/2.0)/2.0-offset_circle,0])
        difference(){
            hull(){
            cylinder(h=height, d=width_circle_out, center=true);
            translate([0,-3*width_circle,0]) cube([flex_bar_width,flex_bar_width,height], center = true);
            translate([0,width_circle+offset_circle,0]) cube([flex_bar_width,flex_bar_width,height], center = true);    
            }
            cylinder(h=height, d=width_circle, center=true);
        }
        translate([0,(flex_bar_length+(width_circle_out+width_circle)/2.0)/2.0+offset_circle,0])
        difference(){
            hull(){
                cylinder(h=height, d=width_circle_out, center=true);
                translate([0,-width_circle-offset_circle,0]) cube([flex_bar_width,flex_bar_width,height], center = true);    
            }
            cylinder(h=height, d=width_circle, center=true);
        }
    }
}
```

### 2.2.4 Result of the final parametric version

![](../images/bistable_parametric.png)

## 2.3 License

I have choosen the license CC BY-NC.

1. CC stands for creative common
2. BY stands for credits must be given to the author
3. NC only non commercial uses of the work are permitted

This license lets others remix, adapt, and build upon your work non-commercially, and although their new works must also acknowledge you and be non-commercial, they don’t have to license their derivative works on the same terms.

## 2.3 Useful links

The best tools to learn openScad:
- [OpenScad online](https://openjscad.com/)
- [OpenScad cheatsheet](https://openscad.org/cheatsheet/)