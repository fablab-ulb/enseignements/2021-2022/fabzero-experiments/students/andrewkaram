$fn=100;

platform_width=1.0;
platform_diameter=100.0;
rotation_support_height=3.5;
rotation_support_diameter=4.8;
rotation_support_cap_ratio=5.0;

offset_circle=0;
width_circle=rotation_support_diameter+0.2;
width_circle_out=width_circle+2.0;
height=rotation_support_height-0.3;
flex_bar_width=1.5;
flex_bar_length=platform_diameter*6.0/8.0;
strum_ray=2.5;
strum_scale=1.5;

translate([0,-37.5,0])

union(){
translate([0,75,0])
    union(){    
//platform
difference(){
cylinder(h=platform_width,d=platform_diameter,center=true);
cylinder(h=platform_width,d=platform_diameter/2.0,center=true);
}
//rotation_support
union(){
    translate([0.0,platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,(rotation_support_height+rotation_support_height/rotation_support_cap_ratio)/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
    translate([0.0,-platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,rotation_support_height/2.0+rotation_support_height/rotation_support_cap_ratio/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
}
}
union(){    
//platform
difference(){
cylinder(h=platform_width,d=platform_diameter,center=true);
cylinder(h=platform_width,d=platform_diameter/2.0,center=true);
}
//rotation_support
union(){
    translate([0.0,platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,(rotation_support_height+rotation_support_height/rotation_support_cap_ratio)/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
    translate([0.0,-platform_diameter/8.0*3.0,platform_width/2.0+rotation_support_height/2.0])
    union(){
        translate([0,0,rotation_support_height/2.0+rotation_support_height/rotation_support_cap_ratio/2.0])
        cylinder(h=rotation_support_height/rotation_support_cap_ratio, d=rotation_support_diameter+0.2, center=true);
        cylinder(h=rotation_support_height, d=rotation_support_diameter, center=true);
    }
}
}
}


/*
    FILE    : flexlink_dual.scad

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-29

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

*/


width=15.8;//mm the 2 extremities
depth=7;
height=3.2;
length_br=65;//mm la branche reliant les 2 extrémités

epaisseur=1;

hole_r=2.5;
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous
translate([60,-80,-0.5])
rotate([0,0,90])
union(){
difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}



translate([width,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};

translate([(width*2)+length_br,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([(width*2)+length_br+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};
}
