#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"
#define I2C_ADDRESS 0x3C
 
SSD1306AsciiAvrI2c oled;
const int analogInPin = A0;  // Analog input pin that the generator is attached to

int sensorValue = 0;        // value read from the generator
int energySum = 0;          // summing values from the generator
int energySumPrev =0;

void setup() {
  oled.begin(&Adafruit128x64, I2C_ADDRESS);
  oled.setFont(fixed_bold10x15);
  oled.set1X();
  oled.println("Quantité d'eau utilisée:");
  oled.set2X();
  oled.println("L");
  oled.setCol(1);
}
void loop() {
  sensorValue = analogRead(analogInPin);
  energySum+= sensorValue;
  if(energySum-energySumPrev>100){ //This allows the screen to not reset all the time
    oled.clearField(0,1,5); 
    energySumPrev=energySum;
    if(energySum>10000){
      oled.println("Stop !");
    }
    else{
      oled.println(energySum);
    }
  }
}
