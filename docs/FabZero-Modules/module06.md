# 6. Electronique 2 - Fabrication

Today we are going to recreate an arduino with one of a new generation chips, the **Atmel SAMD11C**. We are gonna learn to solder **smd** components on a pcb already designed by the fablab and have a brief introduction to what's a bootloader and the component that are necessary to make an arduino board.

## 6.1 Arduino introduction
An **arduino** is simply a **micro controller** that has a **bootloader** on it allowing it to communicate with the arduino IDE, receive its code and execute it. The bootloader is the first program that is gonna be launched everytime an arduino is turned on. This creates a delay, in some cases when a project is finished the bootloader can be removed. This is called burning the bootloader.

## 6.2 Microcontrollers
A microcontroller is a very small computer, it typically contains a CPU, different kind of memories, volatile and non-volatile, and a certain number of input output pins.

Due to the popularity of arduino and all the documentation that was made available by its community, the **atmega328** is maintain in the rankings of most used chips. Eventhough new chips have been developped nowadays that are way more effective.

## 6.3 Soldering advices
Be careful, the components can burn if the temperature raises to a certain value. Don't stay with the iron for a too long time on a same component. The time is actually available for each chips and each composents in their datasheet. The datasheet for the chips we will use is available in section 6.4 in the hyperlink "Atmel SAMD11C data sheet". 

Also never forget that a led has a direction. Solder it in the good direction. I soldered the power led of the chip in the wrong direction. But since the other one is in the good direction and I can make it blink I didn't unsoldered it to resolder it in the good direction. This can be dangerous for the board and tricky to make so be careful before soldering stuffs and make sure they are in the right way.

## 6.4 Create your arduino
 The pcb designed by the fablab has been made in a way that it's plugable in a usb port.

This is the **datasheet** for the micro controller we are gonna use for the soldering:
- [Atmel SAMD11C data sheet](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42363-SAM-D11_Datasheet.pdf)

We are also gonna load the bootloader on the chip and program it from the arduino IDE.

However, there is an issue with the alimentation by the usb port of this chip. Indeed the chip choosen works at 3.3V and the USB port delivers a 5V output. We thus need to use a voltage controller to reduce 5V to 3,3V. Precisely we use a **DC-to-DC converter**, the output of the converted voltage is parallelized with a **capacity** that is gonna act as a stabilizer for high frequency variations. This gives a stable 3.3V alimentation for our chip.

### 6.4.1 Soldering and bootloader step

The first thing to do is to solder the first vital elements of the chip:
1. The chip itselfs.
2. The voltage controller.
3. The capacity in parallel with the output of the voltage controller.

The second thing is to **load the bootloader** and the usb communication on the chip this will help us verify that the chip is well soldered before we move on. 
- [Pin schema of Atmel SAMD11C](https://gitlabsu.sorbonne-universite.fr/fablabsu/fabacademy/samd11c-blinky/-/tree/main)

After this is well installed on the chip let's finish by soldering the last elements. Now we can strictly follow what's describe on the following link:

- [Do your own arduino](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md)

*/!\ Be careful, not to stay too long on a component with the iron or you might burn it. The exact time you can stay on a component is furnished in the documentation of each component. This is an indicative time and typically you can stay a bit longer on a component.*

Once the soldering is over this is what you should get:

![](../images/module6/pcb.jpg)

### 6.4.2 Arduino IDE and settings step
Follow the programing steps described in the "Do your own arduino" hyperlinks taking into account the two sentences below.

Be careful though the only difference is to use this json instead of the previous json mentionned in the previous link: https://raw.githubusercontent.com/qbolsee/ArduinoCore-fab-sam/master/json/package_Fab_SAM_index.json .

And also sometimes you have to select your port manually before uploading the program on the arduino. You go in the tools > port setting and normally, if the arduino is well soldered you'll have to select the usb port. (COM5 or something)


### 6.5 Code Example

The code used to make the led blink is strictly similar to the led blink example in the arduino IDE. The only thing is to select the good pin, here 15. And since there are no led built in remplace it by that number.

```
/*
 *   FILE    : blink.ino
 *   AUTHOR  : Andrew KARAM <karamyo@hotmail.com>
 *   DATE    : 2022-02-28
 *   license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/

const int led = 15;
const int blink_speed = 200;

void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(blink_speed);                       // wait for half a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(blink_speed);                       // wait for half a second
}
```


