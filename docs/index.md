## About me
Hi! I am Andrew Karam. I am a computer engineer student passionated by art research and development and I am also a brazilian jiujitsu practitionner.
![](images/andrew.jpg)

## My background

I was born in Brussels and had an early interest for digital technologies.
Grewing up exclusively in Brussels, where I also began to study engineer, I am currently finishing my master in Computer engineering at Université Libre de Bruxelles.
I have in parallel developped some limited skills in various domain such as music, drawing and 3D modeling.
The first time I touched an arduino, it felt like magic. It was thus natural for me to be so intrigued by this course based in the fablab of the ULB.

