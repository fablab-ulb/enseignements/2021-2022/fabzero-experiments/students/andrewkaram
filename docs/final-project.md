# Final Project

Water, one of the main ecology issue. It is present in at least 3 of the 17 main goals of the ONU in their 2030 Agenda for Sustainable Development.

Individuals cooperations matter, water bad habits needs to stop. One of them can be the time spent in showers. Since water access became so easy, **people tend to not realize the water quantity they waste**. The link between the water consumption and people consciousness need to be reunited. I am convinced that making people aware of their personal waste can enhance ecology reflexes.

![](../docs/images/finalproject/affiche.jpg)

The **final project idea** is a small self-powered device that would go on the base of shower hoses and measure the quantity of water used while showering. The device is equiped of a small o'led screen and would make a small noise whenever someone is going over his personal average use of water and over an ecologic shower threshold of 15 liters. 

This could reunit the consciousness of people with their real consumption.

## Design process

To be successful and to have a real interest, I knew this project needed to be compact and easy to use. None will be interested by this kind of device if it is space consuming or if it needs to be plugged in etc.
Another difficulty of this project is the mix of electronic and water. This slows down the testing and exploring.
And the last important point is to not lose the current water count if someone pause his or her shower. Indeed whenever someone pause the waterflow, the turbine don't aliment the microcontroller and it than stops. If the person turns on the shower 1 minute later, it is supposed to continue counting from the last number and not restart the count.

I came up with 3 solutions for this last problem:
1. Using a battery: There are different kinds of batteries, a type of modern one is named Li-ion battery. However these battery have the disadvantage of needing a constant voltage charge. Some modules have been created specifically for keeping this charging voltage steady but they are wasting energy and we already are low on energy.
2. Using a capacitor: The capacitor is super interesting in this case because it is resistant, has a time duration that can be easily computed with its half life time constant depending on the capacitor capacity and the resistance of the circuit: t1/2 = 0.693 × RC. Which means, if I wanted to have a time of half life equal to 60 seconds, I needed to have an RC product equal to 86.6. The problem with this method is since the half life time to discharge the capacity is 60 seconds, the half life time to charge the capacity will also be 60 seconds and that's something we certainly don't want (we don't want our circuit to start counting the liters after 60 seconds of water use). An idea I had to avoid this was to use a transistor system allowing the circuit to have an equivalent Resistance very low when current is flowing from the turbine to the circuit. The transistor would close when the current is not flowing anymore from the turbine and the equivalent resistance would thus be way higher. This would result in a capacitor having an half life time small when the turbine is feeding it and it's charging, and the same capacitor having an half life time very large when the turbine is not feeding it anymore. Unfortunately I didn't find anything about this method online and I didn't had the material to test the circuits.
3. Using a Real Time Clock: By using an external RTC module, advantages appear. It is now possible to keep track of the real time all the time, so whenever you take a shower the data and time can be added. But how is it helping to keep the shower counter between pauses? By keeping track of the time we can write in non volatile memory the current shower counter and the time. Whenever the microcontroller turns on, it verifies if the last shower it took was less than 1 minute ago, if it is the case than it continues counting from the amount of water he stored. This real time clock is super low current consuming and a very small battery can keep the device turning for years and keeps a very high precision. It is also possible to design a circuit to recharge this very small battery every time the water turbine works by using the extra amount of energy the display doesn't need.

The solution I chosed is the 3rd solution, as you will read in the material and research section. Not only it has advantages I mentionned in the discussion above but it has also the advantage of having a precise temperature measurer which can tell the user the exact temperature of water he is putting on himself.

The other choices I made in this project were by default. The microcontroller is the atmega328p pu because it is the one used in the arduino uno, and the OLED screen because it was available in the fablab.

## Material and research

### Water turbine

The first thing I use is a **water turbine generator** based on an alternator. 

![](../images/finalproject/waterturbine.jpg)

This will furnish the energy for all the circuit. The water turbine is brought up with a circuit that redresses the tension with a 6 diode redressor and a capacity in parallel to smooth out high frequency variations. 

![](../images/finalproject/6diode.png)

![](../images/finalproject/resultantdc.jpg)

The signal then goes through a **tension regulator** 78m05:
- [Tension regulator datasheet](https://www.alldatasheet.com/view.jsp?Searchword=78m05&gclid=CjwKCAjwnZaVBhA6EiwAVVyv9BOuawxrMh2llpgQjWAbTJSGI0lKpRm8S1QvgoqkASYpFopo9-BEZhoCDr0QAvD_BwE)
This allows to convert any signal from 7V to 20V to a 5V signal. I'll use the raw signal entering the tension regulator to have a measure of the rotation speed of the water turbine. This measure will gives us a direct translation to have a measure of how many liters of water are going through the turbine per second. It is important to measure this signal before treatment otherwise no variations can be measured.
The generic formula linking the tension to the rotation speed in an alternator is V = kφω where V is the induced voltage, k includes the size of the machine (the number of poles, their size, the magnet strength, etc) and ω is the rotational speed in radians per second. The tension is thus directly proportional to the tension generated.
An other important step for precision measurement of this tension is to filter the high frequency variations of the signal, this can be done by placing a capacitor in parallel with the raw signal and the earth. This frequency variations can be observed on the image that follows on the green resultant DC waveform. Let's imagine we sample our measure everytime the peak is low, we'd predict less water being consumed than the actual number. This is why it is important to flatten these variations with a capacitor.

![](../images/finalproject/insidewaterturbine.jpg)

### Atmega328p pu microcontroller 

This microcontroller is being picked because I had the comfort of already having one at home. It has an analog pin that allows to read values between 0 and 5V and it maps it to values between 0 and 1023. When you read the raw volt signal of the turbine you have to make sure it doesn't exceed the 5V otherwise the microcontroller might burn. A simple volt divider can do the trick here and a zener diode with a breakdown of 20V, this would mean a volt divider of 4 might be suffisant but since the zener breakdown curve is not straight, the tension can go higher, thus a volt divider of 6 is better for security. It is a security/precision tradeoff.

### DS3231 real time clock 

This module is used by the arduino to have the time, the date and the temperature. All those features are convenient for the project but its main purpose is to prevent the shower counter to reset if someone is taking a break in its shower. To do so we simply store in non volatile memory when the shower stopped last time it was used and if it was less than 2 minutes ago the shower counter restores the previous counter and continue counting the current shower.

This module is widely used on the internet and I realized there was a circuit on its board to recharge the battery of the module automatically when the device is plugged in. 

![](../images/finalproject/DS3231_IIC_real_time_clock.jpg)

After some research I realized this module was wrongly designed and could lead to a malfunctionning of the battery. This is superbly explained in the following link : - [battery charger on ds3231](https://www.onetransistor.eu/2019/07/zs042-ds3231-battery-charging-circuit.html)

In resumee the **simple charging circuit** of this module could work if voltage across the cell can go as high as 4.2 V but never exceed this value. Because fully charged Li-Ion cell voltage must not exceed 4.2 V. Going higher, the battery life is severely reduced and there is the risk of catching fire. Since we aliment this circuit with a constant 5V supply and can't make sure there is this 0.8V drop accross the chip DS3231, the circuit is dangerous. A solution to this would be to use a different voltage regulator at the exit of the water turbine instead of a 78m05, use a regulator that makes the voltage not exceed 4.2V and find a microprocessor that works with this value.

This is why I followed what he recommended and removed the led allowing the recharge process. I also removed the led showing when the module was turned on, to make some current economy. 

Also this module has space in its RAM for two alarms. These register can be used and set to put any value in. This is the space I am going to take advantage from to store the previous shower date and quantity. I got inspired from this forum discussion: - [How to write in the RAM of DS3231](https://forum.arduino.cc/t/ds3231-rtc-module-help-with-ram-usage/268201)

Lastly the ds3231 board is accompanied with an EEPROM memory that is more resilient than the one in the atmega328p pu. This is the memory I am going to use to store the previous showers count and allow an averaging.
  

### OLED VMA 438
The screen I borrowed from the fablab, eventhough it is not the ideal screen, is working well for a prototype. It is an oled screen, those screens have the benefits of being extremely low cost in term of energy. This website mentions a total of 5.6mA for 50% of the grid being turned on: -[Oled current consumption](https://bitbanksoftware.blogspot.com/2019/06/how-much-current-do-oled-displays-use.html). 

The perfectly suited screen for the application should be water resistant and round to fit an electric generator form.

The tactile screens of e-watches such as the apple watch would be perfect with the touch screen removed. (Otherwise this would allow people to modify settings and use the shower longer if the screen was tactile)


## PCB design
The design needed to be compact to fit between the shower hose and the shower head. To do so I decided to learn a way to design PCB's.
I learned Kicad which is an opensource EDA/electronics software. It is not the most easy to and manoeuverable to handle but I managed to learn it fast with the great tutorial of Eric Peronnin in french at the following link:
- [Eric Peronnin kicad's tutorial](https://www.youtube.com/watch?v=2RiGRDuINxE&t=964s&ab_channel=EricPeronnin)

The process is separated in two main steps:

1. The schematic design:
  - To run an atmega328p pu microcontroller that is going to have multiple I2C connection, the stability of it's frequency is primordial, this is controlled by a Quartz crystal, named as Y1 in the schematic. The Quartz Crystal Oscillator produces a train of continuous square wave pulses whose fundamental frequency is controlled by the crystal itself. This fundamental frequency regulates the flow of instructions that controls the processor device. For more detailed information on its role, this link is perfectly detailed and explains the importance of C1 and C2: - [Quartz Crystal](https://www.electronics-tutorials.ws/oscillator/crystal.html).
  - The resistance R2 and R1 are making a tension divisor. Since the atmega328p pu's analog pins are able to read a maximum of 5 volts and after having verified the datasheet of the voltage limitation of my waterturbine generator I know the tension can reach 20 volts before being regulated down to 5 volts. And since I want to measure this tension before it gets regulated, I need to divide this tension by at least 4. By precaution I decided to divide the tension by a bit more than 4, theoricaly 6 with R2=5*R1.
  - SCL, SDA and 5V, Ground these pins are used by both the DS3231 and the oled screen to convey an I2C communication between them and the atmega328p pu. The SCL line is used to set the clock between the microprocessor and the slaves. The SDA line is used to send data from the microprocessor to the oled screen and back and fort between the microprocessor and the DS3231. The 5V and ground lines are used to power both devices. An important fact about the I2C communication is that each device plugged into a conversation are plugged in parallel, which means they share the same wires and data. The atmega328p pu communicate with the devices separately by using a method of addressing the device he wants to have a conversation with before starting exchanging the data. This is well explained in this video: -[I2C communication](https://www.youtube.com/watch?v=15XY4LoQyjc&ab_channel=Comprendrel%27%C3%A9lectronique)

![](../images/finalproject/pcbSchematic.png)

2. The PCB design: 
  - The first step when you have designed your schematic is to load the pcb representation and rearrange each component. Since my ultimate goal is to make it as compact as possible the longest element limiting my size is the atmega328p pu, I could and should have made an analyze of other microcontroller smaller and more suited for the small application i'm using it for. As you may observe the 4 pins for the I2C communication (SCL,SDA,GND,5V) are plugged in parallel.
  - Since I am doing a fairly simple PCB, I only have the use of two sides to make connections. The green connections are in the back layer of the pcb and the red connections are in the front.
![](../images/finalproject/PCB.png)

And this is the result the pcb is looking like when every components are soldered on it.

![](../images/finalproject/PCB3D.png)

## 3D Modeling

Since our PCB size is limited by the atmega328p pu which is 38mm long and by far the longest element, the 3D model must be at least 40mm tall (2mm to give room for the screen and the other displays). The top of the model as a hole drilled in it in the form of the OLED screen and the round border circling it is the place to put the window isolating the inside from the water.

![](../images/finalproject/model3D.png)

![](../images/finalproject/model3D2.png)

I decided to print it in PLA for convenience and for the prototype but PETG is commonly considered as the most convenient waterproof filament.
This is what the result of the 3D print looks like.

![](../images/finalproject/3Dprinted.jpg)

## Soldering

This is the front of the PCB.

![](../images/finalproject/pcb_front.jpg)

This is the back of the PCB.

![](../images/finalproject/pcb_back.jpg)

This is a comparison of the PCB and the chip Atmega328p-pu.

![](../images/finalproject/pcb+chip.jpg)

This is the chip soldered with the crystal.

![](../images/finalproject/pcb_crystal.jpg)

And with the OLED.

![](../images/finalproject/pcb_oled.jpg)

## Result
Unfortunately although a lot of efforts I couldn't figure out what caused my project to not succeed. I have tested everything before I soldered it on the pcb. Even tested the pcb, unfortunately something is making everything bug. I didn't take videos of the screen display I designed for the OLED before I soldered the chip on the pcb.

This is what the device looks like with every elements fitting inside the 3D printed cage.

![](../images/finalproject/result.jpg)

This is a video of the first time I tried the device, with everything mounted inside and soldered as it should have been.
Unfortunately the screen don't light up and after lots of tests the biggest possibility of broken element is the Atmega328p-pu chip.

<video controls muted>
<source src="../images/finalproject/video-1655052943.mp4" type="video/mp4">
</video>

## Future improvements
Adding a very small speaker or a vibration motor (the same than in phones) to alert for two possible things:
1. When the temperature reaches a certain temperature for the first time. This would allow people to not waste water when trying to get it hot before entering their shower.
2. When the shower consumption reaches the average and the ecologic level.

The other big upgrades would be to change chip for a smaller one and a less consuming one. At the same time we could only use the ds3231 chip and make a small working rechargeable battery for it which would allow to keep the correct current time. 

The last improvement would be to simply laser cut a round plexyglass window that would be glued to the top of the device to isolate it from water leaking.

## Conclusion
I might have had a too complex ideas where a lot of new elements had to come out good together. The PCB design is not only a kicad process, it is also very depending on all the settings a website is offering to configure it. Something might have gone wrong in this stage.

The other element that was against me was the time. I ordered the PCB on the 03 of may and received it the 08 of june when the website mentionned it was gonna take between 11 and 16 days. These time consuming problems were due to additional taxes from importation that weren't paid when ordering.

Eventhough during this time all the prototyping on a breadboard was successful and good I didn't thought this was gonna lead to such a non successful result.

With more time, I could have done things better and do non permanent soldering of the chip and test what was going wrong and, I think, succeed this project.

I have learned a lot from these mistakes and got confronted to the hardware issues when I was used to software issues. These are two very distincts way of proceeding, with software you can skip steps and the compiler helps you to solve them. The hardware issues need to be avoided by testing everything consciously and doing non permanent steps one by one.

## Other work aknowledgement
Before working on this topic, I made sure it was untreated before. Unfortunately (or fortunately) a small company has developped a similar product in 2021. I discovered their existence at the end on my project, they are a very new start up with not much of visibility.
Fortunately there are some features that are different, first they don't display the time. With my implementation we could keep an eye on the evolution of the water consumption and its temperature throughout the year and display a diagram for each season. 

The analysis of the result they made is a sum of the shower average before using their product and after using their product. We observe a net difference in the involvement of people in their water consumption. This shows that the idea works.

The design of their product is clearly different they display the water counter on the back of the shower head. This seems unconvenient for people who likes to leave their shower head in place.

![](../images/finalproject/amphiro.png)

The last big difference is the price. They sell it at 95 euros when the price to create one is of **maximum** 27,33 euros with my solution.


# Reproduction feature

## Materials

| Qty |  Description    |  Price  |           Link           | Note  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | OLED screen 128x64    |  3.00 €| https://fr.aliexpress.com/item/33008480580.html?_randl_currency=EUR&_randl_shipto=BE&src=google&src=google&albch=shopping&acnt=708-803-3821&slnk=&plac=&mtctp=&albbt=Google_7_shopping&albagn=888888&isSmbAutoCall=false&needSmbHouyi=false&albcp=11491776376&albag=119062536624&trgt=296730740870&crea=fr33008480580&netw=u&device=c&albpg=296730740870&albpd=fr33008480580&gclid=CjwKCAjwnZaVBhA6EiwAVVyv9HEkSj5_gPVGO1YDvneOEMAkufwPjY_M2JzJZsVao7vD1-PSOSD13RoCn8wQAvD_BwE&gclsrc=aw.ds&aff_fcid=ddd64d79bd644fb19d63f66cff7d39b4-1655051913728-00229-UneMJZVf&aff_fsk=UneMJZVf&aff_platform=aaf&sk=UneMJZVf&aff_trace_key=ddd64d79bd644fb19d63f66cff7d39b4-1655051913728-00229-UneMJZVf&terminal_id=461e0b4eabcc43dd8409d9b75cbcf890&afSmartRedirect=y   |    I borrowed one from the fablab    |
| 1   | Atmega328p-pu    |  2.13 €| https://fr.aliexpress.com/item/1005004098611855.html?spm=a2g0o.productlist.0.0.1b2445f6bLsEGo&ad_pvid=20220612094158231046471353060006997886_3&s=p   |    I already had an arduino uno    |
| 5   | PCB  |  30 €| https://www.pcbway.com/?ngaw=103&campaignid=177159451&adgroupid=9019282171&feeditemid=&targetid=kwd-11018754296&loc_physical_ms=1001004&matchtype=p&network=g&device=c&devicemodel=&creative=347460113410&keyword=fast%20pcb%20prototype&placement=&target=&adposition=&gclid=CjwKCAjwnZaVBhA6EiwAVVyv9KrnY-3qZDI_IjtPvz7R-gpzTEpCzsxVmpJBbHxQrDgMIbQQFV4jyBoC7o0QAvD_BwE   |    The price is 11 euros for delivery and an addition 19 euros from border tax.    |
| 1   | DS3231   |  8.99 €| https://www.amazon.fr/AZDelivery-RTC-gratuite-Raspberry-microcontr%C3%B4leur/dp/B01M2B7HQB/ref=sr_1_1_sspa?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=237L6S4NANZ2A&keywords=ds3231&qid=1655052212&sprefix=ds3231%2Caps%2C78&sr=8-1-spons&psc=1&smid=A1X7QLRQH87QA3&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyU003OElaUFo0TkM5JmVuY3J5cHRlZElkPUEwNDIwNDc3MjFER1JNSklLQVNNVyZlbmNyeXB0ZWRBZElkPUEwMDQ3NDMzMlZPMk9UQzNGWlg1MyZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=   |    Can find way lower prices if you buy only the chip    |
| 1   | Water Turbine  |  6.90 $| https://www.dfrobot.com/product-1610.html   |        |
| 1   | 16Mhz Quartz crystal  |  0.21 €| https://www.digikey.be/nl/products/detail/raltron-electronics/AS-16.000-18/10271769?utm_adgroup=Crystals&utm_source=google&utm_medium=cpc&utm_campaign=Shopping_Product_Crystals%2C%20Oscillators%2C%20Resonators&utm_term=&productid=10271769&gclid=CjwKCAjw46CVBhB1EiwAgy6M4hO8OMAM743OFEL7HR8z4-DsaNCcqKWTCR1PnhMBLnxTYTxb1UG_SBoC7U0QAvD_BwE   |        |

For one reproduction and counting 1 pcb the total cost is 27,33 euros, these prices could and should be way lower if bought in large quantity and if the company the pcb is ordered from is not trying to avoid taxes.

## Code

```
/*
 *  FILE    : code.ino
 *  AUTHOR  : Andrew KARAM <karamyo@hotmail.com>
 *  DATE    : 2022-05-20
 *  license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "RTClib.h"

RTC_DS3231 rtc;
char *daysOfTheWeek[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET 4
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
int loadingbarX=6, loadingbarY=56, loadingbarWidth=116, loadingbarHeight=8;
float literCounter=0;
float literCounterDivider=15;
float averageShower=15.0;
float ecoShower=12.0;
bool cling=true;
int rawVoltPin = A2;
int rawVolt = 0;  // variable to store the value read
unsigned long chipTime=0.0;

void setup() {
  Wire.setClock(300000); //Baudrate augmentation otherwise multiple I2C communication is slow. (standard=100000, atmega328p has a max baudrate of 400000)
  Serial.begin(9600);
  if (! rtc.begin()) {
    Serial.begin(9600);
    Serial.println("Couldn't find RTC");
    while (1);
  }
  if (rtc.lostPower()) {
    Serial.begin(9600);
    Serial.println("RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)// Check your I2C address and enter it here, in Our case address is 0x3C
  display.clearDisplay();
  display.display(); // this command will display all the data which is in buffer
  display.setTextColor(WHITE, BLACK);
  display.drawRect(37, 17, 3, 3, WHITE);     // Put degree symbol ( ° )
  draw_text(42, 17, "C", 1);
  //Draw loading bar
  display.drawRect(loadingbarX, loadingbarY, loadingbarWidth, loadingbarHeight, WHITE);
  char bufferShower[32];
  
  draw_text(68,19,"Shower:",1);
  display.setCursor(68,28);
  display.setTextSize(1);
  display.print("Avg=");
  display.print(averageShower,1);
  display.print("L");
  display.setCursor(68,37);
  display.setTextSize(1);
  display.print("Eco=");
  display.print(ecoShower,1);
  display.print("L");
  // Draw the indicator on the loading bar
  
  if(averageShower>=ecoShower){
    literCounterDivider=averageShower;
    ecoShower=(ecoShower/averageShower*loadingbarWidth-1);
    if(loadingbarWidth>ecoShower+5){
      draw_text(loadingbarWidth+3,47,"A",1);
      draw_text(ecoShower-2,47,"E",1);
      display.fillRect(ecoShower, loadingbarY+1, 1 , loadingbarHeight-2, WHITE);
    }else{
      draw_text(loadingbarWidth+3,47,"A",1);
    }
  }else{
    literCounterDivider=ecoShower;
    averageShower=(averageShower/ecoShower*loadingbarWidth-1);
    if(loadingbarWidth>averageShower+5){
      draw_text(loadingbarWidth+3,47,"E",1);
      draw_text(averageShower-2,47,"A",1);
      display.fillRect(averageShower, loadingbarY+1, 1 , loadingbarHeight-2, WHITE);
    }else{
      draw_text(loadingbarWidth+3,47,"E",1);
    }
  }
}
void loop() {
  rawVolt = analogRead(rawVoltPin);  // read the input pin
  unsigned long prevTime=chipTime;
  chipTime=millis();
  unsigned long difTime=0;
  if(prevTime!=0){
    difTime=chipTime-prevTime;
  }
  literCounter+=difTime*rawVolt/600000.0;
  DateTime now = rtc.now();
  Serial.println(now.second());
  Serial.println(now.minute());
  /*============Display Date=================*/
  draw_text(0,0,daysOfTheWeek[now.dayOfTheWeek()],1);
  char currentDate [16];
  sprintf (currentDate, "%02d/%02d/%d", now.day(), now.month(), now.year()); //add leading zeros to the day and month
  draw_text(68,0,currentDate,1);
  
  /*================Display Time================*/ 
  char buffer [16];
  sprintf (buffer, "%02d:%02d:%02d", now.hour(), now.minute(), now.second());
  draw_text(80,9,buffer,1);
  /*================Display Temp================*/
  display.setCursor(4,17);
  display.setTextSize(1);
  display.print(rtc.getTemperature());
  drawProgressbar(literCounter/literCounterDivider*100);
  display.setCursor(0, 27);
  display.setTextSize(2);
  if(literCounter>99.9){
    display.print("99.9");
  }else{
    display.print(literCounter,1);
  }
  display.print("L");
  display.display();
}

void draw_text(byte x_pos, byte y_pos, char *text, byte text_size) {
  display.setCursor(x_pos, y_pos);
  display.setTextSize(text_size);
  display.print(text);
  display.display();
}
void drawProgressbar(float progress)
{
   progress = progress > 100 ? 100 : progress; // set the progress value to 100
   progress = progress < 0 ? 0 :progress; // start the counting to 0-100
   float bar = ((float)(loadingbarWidth-4) / 100) * progress;
   if(cling && progress==100){
    cling=!cling;
    display.fillRect(loadingbarX+2, loadingbarY+2, bar , loadingbarHeight-4, WHITE);
   }else if(!cling && progress==100){
    cling=!cling;
    display.fillRect(loadingbarX+2, loadingbarY+2, bar , loadingbarHeight-4, BLACK);
   }else{
    display.fillRect(loadingbarX+2, loadingbarY+2, bar , loadingbarHeight-4, WHITE); // initailize the graphics fillRect(int x, int y, int width, int height)
   }
   
}
```
